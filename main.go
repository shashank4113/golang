package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Product struct {
	ProductID    int    `json:"productid"`
	Manufacturer string `json:"manufacturer"`
}

// Handle Function
type fooHandler struct {
	Message string
}

func (f *fooHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(f.Message))
}

// HandleFunc Function

func barHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("bar called"))
}

func main() {
	http.Handle("/foo", &fooHandler{Message: "foo called"})
	http.HandleFunc("/bar", barHandler)
	http.ListenAndServe(":5000", nil)

	//Marshal

	product := &Product{
		ProductID:    1,
		Manufacturer: "Sony",
	}

	productJSON, _ := json.Marshal(product)

	fmt.Println(string(productJSON))

	//UnMarshal

	productJSON1 := `{
		"productid":2,
		"manufacturer": "sony",
	}`

	product1 := Product{}
	err := json.Unmarshal([]byte(productJSON1), &product1)
	if err == nil {
		fmt.Printf("json unmarshal product : %s", product1.Manufacturer)
	}
}
